package com.adaptionsoft.games.uglytrivia;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

class GameShould {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    @BeforeEach
    public void init() {
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void provide() {
        getGameRefacto();
        assertThat(byteArrayOutputStream.toString().trim()).isEqualTo(expectedHistory().trim());
    }

    private Game getGameRefacto() {
        boolean notAWinner;
        Game game = new Game();

        game.add("Chet");
        game.add("Pat");
        game.add("Sue");

        Random rand = new Random(1);

        do {

            int roll = rand.nextInt(5) + 1;
            game.roll(roll);

            if (rand.nextInt(9) == 7) {
                notAWinner = game.wrongAnswer();
            } else {
                notAWinner = game.wasCorrectlyAnswered();
            }


        } while (notAWinner);

        return game;
    }

    private String expectedHistory() {
        return "Chet was added\r\n" +
                "They are player number 1\r\n" +
                "Pat was added\r\n" +
                "They are player number 2\r\n" +
                "Sue was added\r\n" +
                "They are player number 3\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 1\r\n" +
                "Chet's new location is 1\r\n" +
                "The category is Science\r\n" +
                "Science Question 0\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 1 Gold Coins.\r\n" +
                "Pat is the current player\r\n" +
                "They have rolled a 3\r\n" +
                "Pat's new location is 3\r\n" +
                "The category is Rock\r\n" +
                "Rock Question 0\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Pat now has 1 Gold Coins.\r\n" +
                "Sue is the current player\r\n" +
                "They have rolled a 5\r\n" +
                "Sue's new location is 5\r\n" +
                "The category is Science\r\n" +
                "Science Question 1\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Sue now has 1 Gold Coins.\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 5\r\n" +
                "Chet's new location is 6\r\n" +
                "The category is Sports\r\n" +
                "Sports Question 0\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 2 Gold Coins.\r\n" +
                "Pat is the current player\r\n" +
                "They have rolled a 4\r\n" +
                "Pat's new location is 7\r\n" +
                "The category is Rock\r\n" +
                "Rock Question 1\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Pat now has 2 Gold Coins.\r\n" +
                "Sue is the current player\r\n" +
                "They have rolled a 5\r\n" +
                "Sue's new location is 10\r\n" +
                "The category is Sports\r\n" +
                "Sports Question 1\r\n" +
                "Question was incorrectly answered\r\n" +
                "Sue was sent to the penalty box\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 3\r\n" +
                "Chet's new location is 9\r\n" +
                "The category is Science\r\n" +
                "Science Question 2\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 3 Gold Coins.\r\n" +
                "Pat is the current player\r\n" +
                "They have rolled a 3\r\n" +
                "Pat's new location is 10\r\n" +
                "The category is Sports\r\n" +
                "Sports Question 2\r\n" +
                "Question was incorrectly answered\r\n" +
                "Pat was sent to the penalty box\r\n" +
                "Sue is the current player\r\n" +
                "They have rolled a 3\r\n" +
                "Sue is getting out of the penalty box\r\n" +
                "Sue's new location is 1\r\n" +
                "The category is Science\r\n" +
                "Science Question 3\r\n" +
                "Answer was correct!!!!\r\n" +
                "Sue now has 2 Gold Coins.\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 2\r\n" +
                "Chet's new location is 11\r\n" +
                "The category is Rock\r\n" +
                "Rock Question 2\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 4 Gold Coins.\r\n" +
                "Pat is the current player\r\n" +
                "They have rolled a 2\r\n" +
                "Pat is not getting out of the penalty box\r\n" +
                "Sue is the current player\r\n" +
                "They have rolled a 1\r\n" +
                "Sue is getting out of the penalty box\r\n" +
                "Sue's new location is 2\r\n" +
                "The category is Sports\r\n" +
                "Sports Question 3\r\n" +
                "Answer was correct!!!!\r\n" +
                "Sue now has 3 Gold Coins.\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 5\r\n" +
                "Chet's new location is 4\r\n" +
                "The category is Pop\r\n" +
                "Pop Question 0\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 5 Gold Coins.\r\n" +
                "Pat is the current player\r\n" +
                "They have rolled a 4\r\n" +
                "Pat is not getting out of the penalty box\r\n" +
                "Sue is the current player\r\n" +
                "They have rolled a 3\r\n" +
                "Sue is getting out of the penalty box\r\n" +
                "Sue's new location is 5\r\n" +
                "The category is Science\r\n" +
                "Science Question 4\r\n" +
                "Answer was correct!!!!\r\n" +
                "Sue now has 4 Gold Coins.\r\n" +
                "Chet is the current player\r\n" +
                "They have rolled a 1\r\n" +
                "Chet's new location is 5\r\n" +
                "The category is Science\r\n" +
                "Science Question 5\r\n" +
                "Answer was corrent!!!!\r\n" +
                "Chet now has 6 Gold Coins.";
    }
}
