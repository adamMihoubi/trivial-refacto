package com.adaptionsoft.games.uglytrivia.questions;

public class Question {
    private final Category category;
    private final String label;
    private final int number;

    public Question(Category category, String label, int number) {
        this.category = category;
        this.label = label;
        this.number = number;
    }

    public boolean isCategory(Category category) {
        return this.category.equals(category);
    }

    @Override
    public String toString() {
        return String.join(" ", label, String.valueOf(number));
    }
}
