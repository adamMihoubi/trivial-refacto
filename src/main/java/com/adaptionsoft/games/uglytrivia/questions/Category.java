package com.adaptionsoft.games.uglytrivia.questions;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public enum Category {
    SPORTS("Sports", List.of(2, 6, 10)),
    SCIENCE("Science", List.of(1, 5, 9)),
    ROCK("Rock", List.of()),
    POP("Pop", List.of(0, 4, 8));

    private final String value;
    private final Collection<Integer> places;

    Category(String value, Collection<Integer> places) {
        this.value = value;
        this.places = places;
    }

    public String getValue() {
        return value;
    }

    public static Category getCategory(int currentPlace) {
       return Arrays.stream(values())
                .filter(category -> category.places.contains(currentPlace))
                .findAny()
                .orElse(ROCK);
    }
}
