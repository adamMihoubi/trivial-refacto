package com.adaptionsoft.games.uglytrivia.questions;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Questions {
    private final Collection<Question> values;

    public Questions() {
        this.values = IntStream.range(0, 50)
                .mapToObj(index -> Arrays.stream(Category.values())
                        .map(category -> createQuestion(index, category)))
                .flatMap(questionStream -> questionStream)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private Question createQuestion(int index, Category category) {
        return new Question(category,
                String.join(" ", category.getValue(), "Question"),
                index);
    }

    public Optional<Question> askFor(Category category) {
        return values.stream()
                .filter(question -> question.isCategory(category))
                .findFirst()
                .map(this::remove);
    }

    private Question remove(Question question) {
        values.remove(question);
        return question;
    }
}
