package com.adaptionsoft.games.uglytrivia;

import com.adaptionsoft.games.uglytrivia.players.Players;
import com.adaptionsoft.games.uglytrivia.questions.Category;
import com.adaptionsoft.games.uglytrivia.questions.Questions;

public class Game {
    private final Questions questions = new Questions();
    private final Players players = new Players();

    boolean isGettingOutOfPenaltyBox;

    public boolean add(String playerName) {
        players.add(playerName);
        System.out.println(playerName + " was added");
        System.out.println("They are player number " + players.numbers());
        return true;
    }


    public void roll(int roll) {
        System.out.println(players.getCurrentPlayerName() + " is the current player");
        System.out.println("They have rolled a " + roll);

        if (players.isCurrentPlayerhasPenalty()) {
            if (roll % 2 != 0) {
                isGettingOutOfPenaltyBox = true;
                System.out.println(players.getCurrentPlayerName() + " is getting out of the penalty box");
                moveCurrentPlayer(roll);
            } else {
                System.out.println(players.getCurrentPlayerName() + " is not getting out of the penalty box");
                isGettingOutOfPenaltyBox = false;
            }

        } else {
            moveCurrentPlayer(roll);
        }

    }

    private void moveCurrentPlayer(int roll) {
        players.moveCurrentPlayerTo(roll);
        System.out.println(players.getCurrentPlayerName()
                + "'s new location is "
                + players.getCurrentPlayerPlace());
        System.out.println("The category is " + Category.getCategory(players.getCurrentPlayerPlace()).getValue());
        askQuestion();
    }

    private void askQuestion() {
        Category currentCategory = Category.getCategory(players.getCurrentPlayerPlace());
        questions.askFor(currentCategory)
                .ifPresent(System.out::println);
    }


    public boolean wasCorrectlyAnswered() {
        if (players.isCurrentPlayerhasPenalty()) {
            if (isGettingOutOfPenaltyBox) {
                System.out.println("Answer was correct!!!!");
                players.increaseCurrentPlayerPurse();
                System.out.println(players.getCurrentPlayerName()
                        + " now has "
                        + players.getCurrentPlayerPurse()
                        + " Gold Coins.");

                players.changeCurrentPlayer();

                boolean winner = didPlayerWin();
                return winner;
            } else {
                players.changeCurrentPlayer();
                return true;
            }


        } else {

            System.out.println("Answer was corrent!!!!");
            players.increaseCurrentPlayerPurse();
            System.out.println(players.getCurrentPlayerName()
                    + " now has "
                    + players.getCurrentPlayerPurse()
                    + " Gold Coins.");

            boolean winner = didPlayerWin();
            players.changeCurrentPlayer();
            return winner;
        }
    }

    public boolean wrongAnswer() {
        System.out.println("Question was incorrectly answered");
        System.out.println(players.getCurrentPlayerName() + " was sent to the penalty box");
        players.giveCurrentPlayerPenalty();

        players.changeCurrentPlayer();
        return true;
    }


    private boolean didPlayerWin() {
        return players.isCurrentPlayerWin();
    }
}
