package com.adaptionsoft.games.uglytrivia.players;

import java.util.ArrayList;
import java.util.List;

public class Players {
    private int currentPlayer;
    private final List<Player> values = new ArrayList<>();

    public void changeCurrentPlayer() {
        currentPlayer++;
        if (currentPlayer == values.size()) currentPlayer = 0;
    }

    public void add(String name) {
        values.add(new Player(name));
    }

    public String getCurrentPlayerName() {
        return getCurrentPlayer().getName();
    }

    public int numbers() {
        return values.size();
    }

    public void moveCurrentPlayerTo(int place) {
        getCurrentPlayer().moveTo(place);
        if (getCurrentPlayerPlace() > 11) {
            resetCurrentPlayerPlace();
        }
    }

    public void resetCurrentPlayerPlace() {
        getCurrentPlayer().resetPlace();
    }

    public int getCurrentPlayerPlace() {
        return getCurrentPlayer().getPlace();
    }

    public int getCurrentPlayerPurse() {
        return getCurrentPlayer().getPurse();
    }

    public void increaseCurrentPlayerPurse() {
        getCurrentPlayer().increasePurse();
    }

    public void giveCurrentPlayerPenalty() {
        getCurrentPlayer().givePenalty();
    }

    public boolean isCurrentPlayerhasPenalty() {
        return getCurrentPlayer().hasPenalty();
    }

    public boolean isCurrentPlayerWin(){
        return getCurrentPlayer().getPurse() != 6;
    }

    private Player getCurrentPlayer() {
        return values.get(currentPlayer);
    }
}
