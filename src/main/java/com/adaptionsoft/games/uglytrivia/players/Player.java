package com.adaptionsoft.games.uglytrivia.players;

public class Player {
    private final String name;
    private int place;
    private int purse;
    private boolean hasPenalty;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void moveTo(int place) {
        this.place = this.place + place;
    }

    public void resetPlace() {
        this.place = this.place - 12;
    }

    public int getPlace() {
        return place;
    }

    public void increasePurse() {
        purse++;
    }

    public int getPurse() {
        return purse;
    }

    public boolean hasPenalty() {
        return hasPenalty;
    }

    public void givePenalty() {
        this.hasPenalty = true;
    }
}
